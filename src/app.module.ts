import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user/user.entity';
import { HourModule } from './hour/hour.module';
import { HourEntity } from './hour/hour.entity';
import { BlockHourModule } from './block-hour/block-hour.module';
import { BlockHourEntity } from './block-hour/block-hour.entity';
import { ReservationModule } from './reservation/reservation.module';
import { ReservationEntity } from './reservation/reservation.entity';
import { ReportModule } from './report/report.module';

@Module({
  imports: [UserModule, TypeOrmModule.forRoot({
    type: 'mongodb',
    host: 'localhost',
    port: 27017,
    database: 'terms_task',
    entities: [UserEntity, HourEntity, BlockHourEntity, ReservationEntity],
    synchronize: true,
    useNewUrlParser: true
  }), HourModule, BlockHourModule, ReservationModule, ReportModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
