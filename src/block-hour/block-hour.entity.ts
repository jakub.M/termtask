import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from "typeorm";
import { createBlockHourDto } from "src/dto/createBlockHourDto";

@Entity()
export class BlockHourEntity extends BaseEntity{
  
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  date: Date;

  @Column()
  hour: number;

  public static async AddFreeHour(hour: createBlockHourDto): Promise<BlockHourEntity>{
    let newBlockHour = new BlockHourEntity();
    Object.assign(newBlockHour, hour);
    return BlockHourEntity.save(newBlockHour);
  }

}