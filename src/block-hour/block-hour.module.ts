import { Module } from '@nestjs/common';
import { BlockHourController } from './block-hour.controller';
import { BlockHourService } from './block-hour.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlockHourEntity } from './block-hour.entity';

@Module({
  imports: [TypeOrmModule.forFeature([BlockHourEntity])],
  controllers: [BlockHourController],
  providers: [BlockHourService]
})
export class BlockHourModule {}
