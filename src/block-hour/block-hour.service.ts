import { Injectable } from '@nestjs/common';
import { BlockHourEntity } from './block-hour.entity';
import { createBlockHourDto } from 'src/dto/createBlockHourDto';

@Injectable()
export class BlockHourService {

  async AddFreeHour(hour: createBlockHourDto): Promise<BlockHourEntity>{
    return BlockHourEntity.AddFreeHour(hour);
  }
}
