import { Controller, Post, Body, HttpStatus, Res } from '@nestjs/common';
import { BlockHourService } from './block-hour.service';
import { createBlockHourDto } from '../dto/createBlockHourDto';

@Controller('block-hour')
export class BlockHourController {
  constructor(private readonly blockHourService: BlockHourService){}

  @Post()
  async createFreeHour(@Body() req: createBlockHourDto, @Res() res){
    await this.blockHourService.AddFreeHour(req);
    return res.status(HttpStatus.CREATED).send();
  }
}
