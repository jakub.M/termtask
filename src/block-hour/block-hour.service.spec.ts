import { Test, TestingModule } from '@nestjs/testing';
import { BlockHourService } from './block-hour.service';

describe('BlockHourService', () => {
  let service: BlockHourService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BlockHourService],
    }).compile();

    service = module.get<BlockHourService>(BlockHourService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
