import { Test, TestingModule } from '@nestjs/testing';
import { BlockHourController } from './block-hour.controller';

describe('BlockHour Controller', () => {
  let controller: BlockHourController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BlockHourController],
    }).compile();

    controller = module.get<BlockHourController>(BlockHourController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
