import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID } from "typeorm";
import { UserController } from "../user/user.controller";
import { createHourDto } from "../dto/createHourDto";

@Entity('hour')
export class HourEntity extends BaseEntity{
  @ObjectIdColumn()
  id: ObjectID;

  @Column() 
  start: number;

  @Column()
  finish: number;

  public static async ChangeHour(hour: createHourDto):Promise<HourEntity>{
    await HourEntity.delete({});
    let newHour: HourEntity;
    newHour = new HourEntity();
    Object.assign(newHour, hour);
    return await HourEntity.save(newHour);
  }
  
}