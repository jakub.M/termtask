import { Module } from '@nestjs/common';
import { HourController } from './hour.controller';
import { HourService } from './hour.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HourEntity } from './hour.entity';

@Module({
  imports: [TypeOrmModule.forFeature([HourEntity])],
  controllers: [HourController],
  providers: [HourService]
})
export class HourModule {}
