import { Controller, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { HourService } from './hour.service';
import { createHourDto } from '../dto/createHourDto';

@Controller('hour')
export class HourController {
  constructor(private readonly hourService: HourService){}

  @Post()
  async ChangeHour(@Body() req: createHourDto, @Res() res){
    await this.hourService.ChangeHour(req);
    return res.status(HttpStatus.CREATED).send();
  }
}
