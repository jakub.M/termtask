import { Injectable } from '@nestjs/common';
import { createHourDto } from '../dto/createHourDto';
import { HourEntity } from './hour.entity';

@Injectable()
export class HourService {
  public async ChangeHour(hour: createHourDto): Promise<HourEntity>{
    return HourEntity.ChangeHour(hour);
  }
}
