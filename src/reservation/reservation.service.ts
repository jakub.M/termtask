import { Injectable } from '@nestjs/common';
import { createReservationDto } from '../dto/createReservationDto';
import { ReservationEntity } from './reservation.entity';
import { editReservationDto } from '../dto/editReservationDto';
import { userForReservationsDto } from '../dto/userForReservationsDto';

@Injectable()
export class ReservationService {
  async AddReservation(reservation: createReservationDto):Promise<ReservationEntity>{
    return await ReservationEntity.AddReservation(reservation);
  }
  async DeleteReservation(reservationId: editReservationDto):Promise<ReservationEntity>{
    return await ReservationEntity.DeleteReservation(reservationId);
  }
  async GetAll():Promise<ReservationEntity[]>{
    return await ReservationEntity.GetAll();
  }
  async AcceptReservation(reservation: editReservationDto){
    return await ReservationEntity.AcceptReservation(reservation);
  }
  async GetUserReservations(user: userForReservationsDto): Promise<ReservationEntity[]>{
    return await ReservationEntity.GetUserReservations(user);
  }
}
