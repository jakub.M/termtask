import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID } from "typeorm";
import { createReservationDto } from "../dto/createReservationDto";
import { BlockHourEntity } from "../block-hour/block-hour.entity";
import { UserEntity } from "../user/user.entity";
import { editReservationDto } from "../dto/editReservationDto";
import { userForReservationsDto } from "../dto/userForReservationsDto";

@Entity('reservation')
export class ReservationEntity extends BaseEntity{
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  date: Date;

  @Column()
  hour: number;

  @Column()
  title: string;

  @Column()
  accepted: boolean;

  @Column()
  user: UserEntity;

  public static async AddReservation(reservation: createReservationDto): Promise<ReservationEntity>{
    
    let newReservation: ReservationEntity;
    let blockedHour: BlockHourEntity;
    let user: UserEntity;
    
    newReservation = await ReservationEntity.findOne({date: reservation.date, hour: reservation.hour})
    blockedHour = await BlockHourEntity.findOne({date: reservation.date, hour: reservation.hour}) 
    user = await UserEntity.findOne({login: reservation.login})

    if(newReservation || blockedHour || !user){
      return null;
    } else{
      newReservation = new ReservationEntity();
      delete reservation.login;
      Object.assign(newReservation, reservation);
      newReservation.accepted = false;
      newReservation.user = user;
      return await ReservationEntity.save(newReservation)
    }
  }

  public static async DeleteReservation(reservation: editReservationDto):Promise<ReservationEntity>{
    let reservationToDelete: ReservationEntity;
    reservationToDelete = await ReservationEntity.findOne({date: reservation.date, hour: reservation.hour});
    return await ReservationEntity.remove(reservationToDelete);
  }

  public static async GetAll():Promise<ReservationEntity[]>{
    const reservations: ReservationEntity[] = await ReservationEntity.find();
    if (reservations.length > 0) return reservations;
    else throw new TypeError('Collection is empty');
  }

  public static async AcceptReservation(reservation: editReservationDto): Promise<ReservationEntity>{
    await ReservationEntity.update({date: reservation.date, hour: reservation.hour}, {accepted: true});
    return await ReservationEntity.findOne({date: reservation.date, hour: reservation.hour});
  }

  public static async GetUserReservations(user: userForReservationsDto): Promise<ReservationEntity[]>{
    const User : UserEntity = await UserEntity.findOne({login: user.login})
    const reservations : ReservationEntity[] = await ReservationEntity.find({user: User});
    if (reservations.length > 0) return reservations;
    else throw new TypeError(`No user's reservations`);
  }

}