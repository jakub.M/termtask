import { Controller, Post, Body, Res, HttpStatus, Delete, Get, Patch } from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { createReservationDto } from '../dto/createReservationDto';
import { ReservationEntity } from './reservation.entity';
import { editReservationDto } from '../dto/editReservationDto';
import { userForReservationsDto } from '../dto/userForReservationsDto';

@Controller('reservation')
export class ReservationController {
  constructor(private readonly reservationService: ReservationService){}

  @Post()
  async createReservation(@Body() req: createReservationDto, @Res() res){
    await this.reservationService.AddReservation(req).then(reservation =>{
      if(reservation) return res.status(HttpStatus.CREATED).send();
      else res.status(HttpStatus.BAD_REQUEST).send('Term is reserved');
    })
  }
  @Delete()
  async deleteReservation(@Body() req: editReservationDto): Promise<ReservationEntity>{
    return await this.reservationService.DeleteReservation(req);
  }
  @Get()
  async getAllReservations(@Res() res){
    const reservations: ReservationEntity[] = await this.reservationService.GetAll();
    return res.status(HttpStatus.OK).send(reservations);
  }
  @Patch()
  async acceptReservation(@Body() req: editReservationDto, @Res() res){
    const reservation: ReservationEntity = await this.reservationService.AcceptReservation(req);
    if (reservation.accepted) return res.status(HttpStatus.OK).send('modified');
    else return res.status(HttpStatus.NOT_MODIFIED).send('Not modified');
  }
  @Get('user')
  async getAllUserReservations(@Body() req: userForReservationsDto, @Res() res){
    const reservations: ReservationEntity[] = await this.reservationService.GetUserReservations(req);
    return res.status(HttpStatus.OK).send(reservations); 
  }
}
