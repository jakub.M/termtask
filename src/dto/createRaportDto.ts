import { IsDateString } from "class-validator";

export class createRaportDto{
  @IsDateString()
  dateFrom: Date;

  @IsDateString()
  dateTo: Date;
}