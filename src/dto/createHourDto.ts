import { IsNumber, Min, Max } from 'class-validator';

export class createHourDto{
  @IsNumber()
  @Min(0)
  @Max(24)
  start: number

  @IsNumber()
  @Min(0)
  @Max(24)
  finish: number
}