import { IsDateString, IsNotEmpty, IsNumber, Min, Max } from "class-validator";

export class createBlockHourDto{
  @IsDateString()
  @IsNotEmpty()
  date: Date;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(24)
  hour: number;
}