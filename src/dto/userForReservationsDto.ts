import { IsNotEmpty, IsEmail } from "class-validator";

export class userForReservationsDto{
  @IsNotEmpty()
  @IsEmail()
  login: string;
}