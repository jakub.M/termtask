import { IsEmail, MinLength, IsString } from 'class-validator';

export class createUserDto{
  @IsEmail()
  login: string;

  @IsString()
  @MinLength(6)
  password: string;
}