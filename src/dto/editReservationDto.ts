import { IsNotEmpty, Min, Max } from "class-validator";

export class editReservationDto{
  @IsNotEmpty()
  date: Date;
  
  @IsNotEmpty()
  @Min(0)
  @Max(24)
  hour: number;
}
