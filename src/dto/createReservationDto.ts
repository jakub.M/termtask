import { IsDateString, IsNumber, Min, Max, IsAlphanumeric, MaxLength, IsEmail } from "class-validator";

export class createReservationDto{
  @IsDateString()
  date: Date;

  @IsNumber()
  @Min(0)
  @Max(24)
  hour: number;

  @IsAlphanumeric()
  @MaxLength(255)
  title: string;

  @IsEmail()
  login: string;
}
