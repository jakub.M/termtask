import { Controller, Get, Res, Body, HttpStatus } from '@nestjs/common';
import { HourEntity } from '../hour/hour.entity';
import { ReservationEntity } from '../reservation/reservation.entity';
import { BlockHourEntity } from '../block-hour/block-hour.entity';
import { createRaportDto } from '../dto/createRaportDto';

@Controller('report')
export class ReportController {

  

  @Get()
  async getReport(@Body() range: createRaportDto, @Res() res){
    let workTime : HourEntity = await HourEntity.findOne();
    let reservations : ReservationEntity[] = await ReservationEntity.find();
    let blockedHours : BlockHourEntity[] = await BlockHourEntity.find();

    const workHour : number = workTime.finish - workTime.start;
    const startDay : number = new Date(range.dateFrom).getTime();
    const finishDay : number = new Date(range.dateTo).getTime();
    
    let report : object = new Object();
    console.log('object')
    for(let day = startDay; day <= finishDay; day += (1000 * 60 * 60 * 24)){
      let currentDate = new Date(day);
      let countReservations : number = reservations.filter(x => new Date(x.date).toDateString() == currentDate.toDateString()).length;
      let countBlockedHours : number = blockedHours.filter(x => new Date(x.date).toDateString() == currentDate.toDateString()).length;
      let countFreeHours : number = workHour - (countBlockedHours + countReservations);
      let reportItem = new ReportPosition(countReservations, countFreeHours, countBlockedHours);
      report = {...report , [currentDate.toDateString()] : reportItem}
    }

    if(report) return  res.status(HttpStatus.OK).send(JSON.stringify({...report}));
    else res.status(HttpStatus.CONFLICT).send('empty raport');
  }
}


class ReportPosition{
  reservations: number;
  freeHours: number;
  blockedHours: number;

  constructor(reservation: number, freeHours: number, blockedHours: number){
    this.reservations = reservation;
    this.freeHours = freeHours;
    this.blockedHours = blockedHours
  }
}
