import { Column, Entity, ObjectID, ObjectIdColumn, BaseEntity } from "typeorm";
import { IsEmail } from "class-validator";
import * as crypto from 'crypto';
import { createUserDto } from "../dto/createUserDto";

@Entity('user')
export class UserEntity extends BaseEntity{
  
  @ObjectIdColumn()
  id: ObjectID;

  @IsEmail()
  @Column()
  login: string;

  @Column({
    select: false,
    name:'password'
  })
  passwordHash: string;

  set password(password: string){
    this.passwordHash = crypto.createHmac('sha256', password).digest('hex');
  }

  public static async Insert(user: createUserDto): Promise<UserEntity>{
    let newUser: UserEntity;
    newUser = await UserEntity.findOne({login: user.login})
    if (newUser) {
      throw new TypeError('User exist')
    } else {
      newUser = new UserEntity()
      Object.assign(newUser, user)
      await UserEntity.save(newUser);
      return new UserEntity();
    }
  }

  public static async AuthUser(user: createUserDto): Promise<UserEntity>{
    let authUser: UserEntity;
    authUser = await UserEntity.findOne({
      select: ['id', 'login'],
      where: {
        login: user.login,
        passwordHash: crypto.createHmac('sha256', user.password).digest('hex')
      }
    });
    return authUser;
  }
  
}
