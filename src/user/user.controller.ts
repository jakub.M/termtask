import { Controller, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { createUserDto } from '../dto/createUserDto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {

  constructor(private readonly userService: UserService){};

  @Post()
  async create(@Body() req: createUserDto, @Res() res){
    await this.userService.createUser(req);
    return res.status(HttpStatus.CREATED).send('User created');
  }

  @Post('login')
  async authUser(@Body() req: createUserDto, @Res() response){
    await this.userService.authUser(req).then(user => {
      if (user)  return response.status(HttpStatus.ACCEPTED).send('Password and login correct');
      else  return response.status(HttpStatus.UNAUTHORIZED).send('Password and login incorrect')
    })
  }
}
