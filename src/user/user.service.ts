import { Injectable } from '@nestjs/common';
import { createUserDto } from '../dto/createUserDto';
import { UserEntity } from './user.entity';

@Injectable()
export class UserService {
    public async createUser(user: createUserDto): Promise<UserEntity>{
      return await UserEntity.Insert(user);
    }

    public async authUser(user: createUserDto): Promise<UserEntity>{
      return await UserEntity.AuthUser(user);
    }
}
